/*
 * aruco.cpp
 *
 *  Created on: 07.03.2016
 *      Author: sasha
 */
// OpenCV stuff
#include "opencv2/videoio/videoio.hpp"
#include <opencv2/highgui.hpp>
#include <opencv2/aruco.hpp>
#include <opencv2/viz.hpp>
#include <vector>
#include <iostream>

using namespace cv;
using namespace std;
const char* mainWindowName = "Наложение_объемного_объекта_на_метку_Aruco";

static bool readCameraParameters(string filename, Mat &camMatrix,
		Mat &distCoeffs) {
	FileStorage fs(filename, FileStorage::READ);
	if (!fs.isOpened())
		return false;
	fs["camera_matrix"] >> camMatrix;
	fs["distortion_coefficients"] >> distCoeffs;
	return true;
}

static bool readDetectorParameters(string filename,
		Ptr<aruco::DetectorParameters> &params) {
	FileStorage fs(filename, FileStorage::READ);
	if (!fs.isOpened())
		return false;
	fs["adaptiveThreshConstant"] >> params->adaptiveThreshConstant;
	fs["minMarkerPerimeterRate"] >> params->minMarkerPerimeterRate;
	fs["maxMarkerPerimeterRate"] >> params->maxMarkerPerimeterRate;
	fs["polygonalApproxAccuracyRate"] >> params->polygonalApproxAccuracyRate;
	fs["minDistanceToBorder"] >> params->minDistanceToBorder;
	fs["cornerRefinementWinSize"] >> params->cornerRefinementWinSize;
	fs["cornerRefinementMaxIterations"]
			>> params->cornerRefinementMaxIterations;
	fs["cornerRefinementMinAccuracy"] >> params->cornerRefinementMinAccuracy;
	fs["markerBorderBits"] >> params->markerBorderBits;
	fs["perspectiveRemovePixelPerCell"]
			>> params->perspectiveRemovePixelPerCell;
	fs["perspectiveRemoveIgnoredMarginPerCell"]
			>> params->perspectiveRemoveIgnoredMarginPerCell;
	fs["maxErroneousBitsInBorderRate"] >> params->maxErroneousBitsInBorderRate;
	return true;
}
void myVizKeyboardEvent() {
	return;
}
void RemoveSub(string& sInput, const string& sub) {
	string::size_type foundpos = sInput.find(sub);
	if (foundpos != string::npos)
		sInput.erase(sInput.begin() + foundpos,
				sInput.begin() + foundpos + sub.length());
}
int main(int argc, char** argv) {

	//////////////// camera parametrs ////////////////////////
	Mat camMatrix, distCoeffs;
	assert(
			readCameraParameters("out_camera_data.xml", camMatrix, distCoeffs)
					!= 0);
	VideoCapture camera(CV_CAP_ANY);

	/////////////// viz core  ///////////////////////////////
	viz::Viz3d myWindow(mainWindowName); /// Create a window
	/// camera
	// go to r0.0.0 t0.0.0
	myWindow.setViewerPose(Affine3d());

	// point cloud
	string modelPath = string(argv[0]);
	RemoveSub(modelPath, "Release/cv_aruco");
	modelPath += "models/wings-fighter.obj";
	Mat cloud = viz::readCloud(modelPath);
	cv::Mat colors(cloud.size(), CV_8UC3);
	theRNG().fill(colors, RNG::UNIFORM, 50, 255);
	viz::WCloud cloudWidget(cloud, colors);
	myWindow.showWidget("Cloud", cloudWidget);

	cout << cloudWidget.getPose().matrix << endl;
	cout << myWindow.getViewerPose().matrix << endl;

	//////////////// aruco detector params /////////////////////////
	Ptr<aruco::DetectorParameters> detectorParams =
			aruco::DetectorParameters::create();
	assert(readDetectorParameters("detector_params.yml", detectorParams) != 0);
	detectorParams->doCornerRefinement = true; // do corner refinement in markers

	//////////////// aruco dictionary //////////////////////////////
	Mat markerImage;
	float markerLength = 0.1;
	Ptr<aruco::Dictionary> dictionary = aruco::getPredefinedDictionary(
			aruco::PREDEFINED_DICTIONARY_NAME(aruco::DICT_ARUCO_ORIGINAL));
	aruco::drawMarker(dictionary, 68, 200, markerImage, 1);
	//Вывоз изображения маркера в окно с наванием "marker"
	imshow("marker", markerImage);

	/////////////// aruco env  ///////////////////////////////
	vector<int> ids;
	vector<vector<Point2f> > corners, rejected;
	vector<Vec3d> rvecs, tvecs;
	/////////////// cv env  ///////////////////////////////
	Mat frame;
	char key;

	while (camera.grab()) {

		camera.retrieve(frame);

		aruco::detectMarkers(frame, dictionary, corners, ids, detectorParams,
				rejected);

		if (ids.size() > 0) {
			aruco::estimatePoseSingleMarkers(corners, markerLength, camMatrix,
					distCoeffs, rvecs, tvecs);

			aruco::drawDetectedMarkers(frame, corners, ids);
			aruco::drawAxis(frame, camMatrix, distCoeffs, rvecs, tvecs,
					markerLength * 0.5f);

			Affine3d markerPose(rvecs[0], tvecs[0]);

			myWindow.showWidget("Cloud", cloudWidget, markerPose);
		}
		myWindow.setBackgroundTexture(frame);
		myWindow.spinOnce(10, true);
		key = (char) waitKey(50);
		if ((char) key == 27)
			break; // Esc to exit...
	}
	return 0;
}

